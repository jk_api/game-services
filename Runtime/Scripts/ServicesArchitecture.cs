using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.Framework;

public class ServicesArchitecture : Architecture<ServicesArchitecture>
{
    protected override void Init()
    {
        this.RegisterSystem(new AnalyticSystem());
        this.RegisterSystem(new AdsSystem());
    }
}