using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.Framework;
using UnityEngine.Events;

public class AdsSystem : AbstractSystem
{
    protected override void OnInit()
    {
    }
    public static void ShowBanner()
    {

    }

    public static void HideBanner()
    {

    }

    public static void ShowAppOpen(string pos, UnityAction callback = null)
    {

    }
    public static void ShowInterstitial(string location, UnityAction callback = null)
    {

    }
    public static void ShowRewardedVideo(string location, UnityAction<bool> callback)
    {

    }
}