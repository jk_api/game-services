using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.Framework;

public class AnalyticSystem : AbstractSystem
{

    protected override void OnInit()
    {

    }
    protected void Log(string message)
    {
        Debug.Log(message);
    }
    public static void LogEvent(string message)
    {
        ServicesArchitecture.Interface.GetSystem<AnalyticSystem>().Log(message);
    }
}